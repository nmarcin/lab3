package com.company;

public class Calculations {
    public Calculations() {
    }

    public static Point2D positionGeometricCenter(Point2D[] point) {
        double X = 0.0D;
        double Y = 0.0D;

        for(int i = 0; i < point.length; ++i) {
            X += point[i].getX();
            Y += point[i].getY();
        }

        return new Point2D(X / (double)point.length, Y / (double)point.length);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double x = 0.0D;
        double y = 0.0D;
        double mass = 0.0D;

        for(int i = 0; i < materialPoint.length; ++i) {
            x += materialPoint[i].getX() * materialPoint[i].getMass();
            y += materialPoint[i].getY() * materialPoint[i].getMass();
            mass += materialPoint[i].getMass();
        }

        return new MaterialPoint2D(x / mass, y / mass, mass);
    }
}
package com.company;

public class MaterialPoint2D extends Point2D {
    private double mass;

    public MaterialPoint2D(double x, double y, double mass) {
        super(x, y);
        this.mass = mass;
    }

    public double getMass() {
        return this.mass;
    }

    public String toString() {
        double var10000 = super.getX();
        return "MaterialPoint2D{x=" + var10000 + "y" + super.getY() + "mass=" + this.mass + "}";
    }
}

package com.company;

class Main {

    public static void main(String[] args) {
        final int POINTS = 2;
        Point2D[] points = new Point2D[2];
        MaterialPoint2D[] materialPoints = new MaterialPoint2D[2];
        points[0] = new Point2D(0.0D, 0.0D);
        points[1] = new Point2D(10.0D, 10.0D);
        materialPoints[0] = new MaterialPoint2D(0.0D, 0.0D, 10.0D);
        materialPoints[1] = new MaterialPoint2D(10.0D, 10.0D, 100.0D);
        Point2D geometricCenter = Calculations.positionGeometricCenter(points);
        Point2D massCenter = Calculations.positionCenterOfMass(materialPoints);
        System.out.println("Polozenie srodka masy: " + massCenter);
        System.out.println("Polozenie srodka geometrycznego: " + geometricCenter);
    }
}
